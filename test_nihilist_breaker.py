import nihilist_crypto as NCsystem
import nihilist_breaker as NCbreaker


# Config
TESTFILE = "testcases.txt"
TESTWORDS = ["star", "klar", "sun", "planet", "PATHFINDER", "DUMBWAITER",
             "BLACKHORSE", "flamethrowing", "ambidextrously", "uncopyrightables"]
TESTKEYS = ["secret", "password", "pwassord", "queen", "king", "jackle", "jocker",
            "canyoufindthis", "yougotthisonerigthicommendyourablilitytocrackciphers", "Hungry"]


def load_test_cases(filename: str):
    with open(filename, 'r', encoding="UTF-8") as f:
        content = f.readlines()

    return content


def setup_test(iteration: int):
    raw_testcases = load_test_cases(TESTFILE)
    testcases = {}
    texy_list = []
    keyword = TESTWORDS[iteration]
    key = TESTKEYS[iteration]
    index = 0
    for case in raw_testcases:
        case = NCsystem.clean(case.strip())
        print("\nTESTING ENCRYPTION\n")
        try:
            result = NCsystem.Encrypt(
                plaintext=case, keyword=keyword, secret_key=key)
            print(f"SUCCESSFUL!")
        except:
            print(f"FAILED!")
            continue

        print("\nTESTING DECRYPTION\n")
        try:
            plaintext = NCsystem.Decrypt(
                cipher=result, keyword=keyword, key=key)

            if plaintext != case:
                print("DECRYPTION WRONG!")
                continue
            print("SUCCESSFUL!")
        except:
            print("FAILED!")
            continue

        testcases[index] = result
        texy_list.insert(index, case)
        index += 1

    return testcases, texy_list


def test_case(cipher: list, plaintext: str):
    # Step 1
    print("\n- GENERATION COMBINATIONS", end='')
    try:
        dictionary = NCbreaker.GenerateCombinations(cipher)
    except:
        print("- FAILURE!")
        return ''

    # Step 2
    print("\n- FIND INDEX OF OCCURENCE", end='')
    try:
        result, keyLen = NCbreaker.run_FindIC(cipher, dictionary)
    except:
        print(f"- FAILURE!")
        return ''

    # # Step 3
    print("\n- GENERATING KEY LIST", end='')
    try:
        keyList = NCbreaker.GetKeyBlend(result, keyLen)
    except:
        print(f"- FAILIURE!")
        return ''
    try:
        keyList = NCbreaker.FilterKeys(keyList, cipher)
    except:
        print(f"- FAILURE!")
        return ''

    # Step 4
    print("\n- RECONSTRUCTING POLYBIUS", end='')
    try:
        poly, key = NCbreaker.PolybiusCracker(cipher, plaintext, keyList)
    except:
        print("- FAILURE!")
        return ''

    clearkey = ''
    for x in range(keyLen):
        clearkey += NCsystem.rev_scan_matrix(str(key[x]), poly)

    return clearkey, poly



def run_test(n: int):

    for i in range(n):
        testinputs, case_list = setup_test(i)

        for index, text in testinputs.items():
            print(f"\n\nTest #{index}  Case: {case_list[index]}")
            clearkey, poly = test_case(text, case_list[index])
            if clearkey != '':
                print(f"\n-- Test #{index} successful --")
                print(f"secret key used : {clearkey}")
                print("Polybius square used:")
                for row in poly:
                    print(row)
            else:
                (f"\n -- Test #{index} Failed --\n")



def interface():
    print("\t Running tests on nihilist_breaker.py and nihilist_crypto.py")
    aws = int(input("Number of test iterations: "))
    for n in range(aws+1):
        run_test(n)


if __name__ == '__main__':
    interface()

