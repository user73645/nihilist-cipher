from math import ceil


def make_polybius_square(key: list) -> list:
    alf, arr = "abcdefghiklmnopqrstuvwxyz", []
    for char in list(alf):
        if char not in key:
            key.append(char)
    L, R = 0, 5
    while len(arr) < 5:
        arr.append(key[L:R])
        L += 5
        R += 5
    return arr


def scan_matrix(char: str, matrix: list) -> str:
    for x in range(len(matrix)):
        for y in range(len(matrix[x][:])):
            if char in matrix[x][y]:
                return str(x+1) + str(y+1)


def rev_scan_matrix(cord: str, matrix: list):
    for x in range(len(matrix)):
        for y in range(len(matrix[x][:])):
            if (int(cord[0])-1 == x) and (int(cord[1])-1 == y):
                return matrix[x][y]


def clean(text: str) -> str:
    l, whitelist = text.lower(), 'abcdefghijklmnopqrstuvwxyz'
    for x in l:
        l = l.replace(x, '') if x not in whitelist else l  # clear bad chars
        l = l.replace(x, 'i') if x == 'j' else l  # Replace j:s with i:s
    return l


def Decrypt(cipher: list, keyword: str, key: str):
    polybius, plaintext = make_polybius_square(list(keyword)), []
    key_list = [scan_matrix(char, polybius) for char in key]  # Key to polybius
    key_list = list(map(int, key_list))             # Make elements integers
    key_list += key_list * ceil(len(cipher)/len(key_list))  #
    while len(key_list) > len(cipher):
        del key_list[-1]
    sub_cipher = list(map(lambda x, y: x-y, cipher, key_list))
    for cord in sub_cipher:
        plaintext.append(rev_scan_matrix(str(cord), polybius))
    return "".join(plaintext)


def Encrypt(plaintext: str, keyword: str, secret_key: str) -> list:
    polybius, ret = make_polybius_square(list(keyword)), []  # Shuffle Polybius
    while len(plaintext) > len(secret_key):
        secret_key += secret_key            # Loop key until plaintext ends
    for n in range(len(plaintext)):
        ret.append(int(scan_matrix(plaintext[n], polybius)) +  # Add key to
                   int(scan_matrix(secret_key[n], polybius)))  # plaintext
    return ret
