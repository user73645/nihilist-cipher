import nihilist_crypto as crypto
import nihilist_breaker as analys
import time

def main():
	klartext = input("Skriv en klartext: ")
	keyword = input("Skriv ett nyckelord: ")
	hemlig_nyckel = input("Skriv en hemlig nyckel: ")

	starttime = time.perf_counter()

	# Funktioner
	ciphertext = crypto.Encrypt(plaintext=klartext, keyword=keyword, secret_key=hemlig_nyckel)
	dict_ = analys.GenerateCombinations(cipher=ciphertext)
	result, length = analys.run_FindIC(cipher=ciphertext, dictionary=dict_)
	KeyList = analys.GetKeyBlend(IC_raw=result, keyLen=length)
	KeyList = analys.FilterKeys(keyList=KeyList, cipher=ciphertext)
	cleanklartext = crypto.clean(klartext)
	result_poly, result_key = analys.PolybiusCracker(cipher=ciphertext, plaintext=cleanklartext, keyList=KeyList)

	executiontime = (time.perf_counter() - starttime)

	print(f"\n Klartext: {klartext}")
	print(f"Kryptogram: {ciphertext} \n")

	print(" - Kryptoanalys - ")
	print("Polybius hittad: ")
	for row in result_poly:
		print(row)

	clearkey = ''
	for x in range(length):
		clearkey += crypto.rev_scan_matrix(str(result_key[x]), result_poly)
	print(f"Hemliga nyckel hittad: {clearkey}")
	print(f"Tidsförlopp: {executiontime}")

if __name__ == "__main__":
	main()
