from nihilist_crypto import Encrypt, rev_scan_matrix, clean, make_polybius_square
from math import ceil
import itertools as it


def FindCombinations(target: int) -> list:
    usableTerms, combinations = [11, 12, 13, 14, 15, 21, 22, 23, 24, 25, 31,
                                 32, 33, 34, 35, 41, 42, 43, 44, 45, 51, 52, 53, 54, 55], []
    for n in usableTerms:
        for m in usableTerms:
            if (n+m) == target:
                combinations.append((n, m))
    return combinations


def Filtering(lis: list) -> list:
    for tup1 in lis:
        tup2 = lis[-1]
        if tup1[0] == tup2[1] and tup1[1] == tup2[0]:
            if tup1[0] != tup2[0]:
                lis.remove(tup2)
    return lis


def GenerateCombinations(cipher: list) -> dict:
    allCombinations = map(FindCombinations, cipher)
    allCombinations = list(map(Filtering, allCombinations))
    subsets = []
    for sublist in allCombinations:
        subsets.append({item for tup in sublist for item in tup})
    dictionary = {key: value for (key, value) in zip(cipher, subsets)}
    return dictionary


def FindIC(cipher: list, combinations: dict, keyLen: int):
    cipherBlocks = [cipher[i:i+keyLen] for i in range(0, len(cipher), keyLen)]
    while len(cipherBlocks[-1]) < keyLen:
        cipherBlocks[-1].append("00")
    IndexBlock = []
    for i in range(len(cipherBlocks[0])):
        x = [item[i] for item in cipherBlocks]
        IndexBlock.append(x)
    IC_result = []
    for block in IndexBlock:
        if "00" in block:
            block.remove("00")
        IC = list(map(lambda x: combinations[x], block))
        intersect = set.intersection(*IC)
        IC_result.append(intersect)
        for sets in IC_result:
            if len(sets) == 0:
                return []
    return IC_result


def run_FindIC(cipher: list, dictionary: dict):
    for keyLen in range(1, 100):
        try:
            result = FindIC(cipher, dictionary, keyLen)
            if not result:
                continue
            return result, keyLen
        except:
            continue
    print("key length does not exist or excceds 100")


def GetKeyBlend(IC_raw: list, keyLen: int):
    IC = [item for sublist in IC_raw for item in sublist]
    result = it.combinations(IC, keyLen)
    keyList = []
    for elm in result:
        keyList.append(list(elm))
    return keyList


def FilterKeys(keyList: list, cipher: list) -> list:
    newkeyList = []
    for key in keyList:
        while len(key) < len(cipher):
            key += key
        while len(key) > len(cipher):
            del key[-1]

        sub_cipher = map(lambda x, y: x-y, cipher, key)
        sub_cipher = filter(lambda x: (x > 10), sub_cipher)
        sub_cipher = filter(lambda x: (x < 56), sub_cipher)
        sub_cipher = filter(lambda x: (x % 10) > 0, sub_cipher)
        sub_cipher = filter(lambda x: (x % 10) < 6, sub_cipher)

        if len(list(sub_cipher)) != len(cipher):
            continue
        newkeyList.append(key)
    return newkeyList


# Step 4

def PolybiusCracker(cipher: list, plaintext: list, keyList: list):
    hidden_content, hopefuls = list("XXXXXXXXXXXXXXXXXXXXXXXXX"), []

    for key in keyList:
        unkown_polybius, i = make_polybius_square(hidden_content), 0
        sub_cipher = map(lambda x, y: x-y, cipher, key)
        sub_cipher = list(map(str, sub_cipher))

        for n in sub_cipher:
            for x in range(len(unkown_polybius)):
                for y in range(len(unkown_polybius[x][:])):
                    if (int(n[0])-1 == x) and (int(n[1])-1 == y):
                        unkown_polybius[x][y] = plaintext[i]
                        i += 1
        hopefuls.append(unkown_polybius)

    temp, k = 25, 0
    for x in hopefuls:
        f, k = sum(x, []), keyList[hopefuls.index(x)]  # Flatten polybius
        length = len(f) - len(set(f))  # Count duplicates
        if length < temp:   # Find polybius with fewest duplicates
            temp = length
            best, key_used = x, k  # Record key used to get polybius
    return best, key_used
