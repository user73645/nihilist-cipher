from nihilist_breaker import GenerateCombinations, run_FindIC, GetKeyBlend, FilterKeys, PolybiusCracker
from nihilist_crypto import Encrypt, rev_scan_matrix, clean, make_polybius_square


with open('WhyIAmSoSmort.txt', 'r') as f:  # Read file with plaintext
    plaintext = clean(f.read())
cipher = Encrypt(plaintext=plaintext,
                 keyword="zyxwvutsrqponmlkihgfedcba",
                 secret_key="confetti")

# Step 1: Generate all possible additions made during stage 2 of encryption
dictionary = GenerateCombinations(cipher)

# Step 2: Filter additions to only contain likely candidates
result, keyLen = run_FindIC(cipher, dictionary)
keyList = GetKeyBlend(result, keyLen)
keyList = FilterKeys(keyList, cipher)

# # Step 3: Verify key and find correct polybius square
polybius, secret_key = PolybiusCracker(cipher, plaintext, keyList)

print("Estimated polybius used:")
for x in polybius:
    print(x)
key = ''
print("Estimated key used:")
for x in range(keyLen):  # Undo key repetition
    key += rev_scan_matrix(str(secret_key[x]), polybius)
print(key)
